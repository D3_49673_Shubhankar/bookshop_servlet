package Servlet;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BookDao;
import pojo.Book;
@WebServlet("/admin")
public class BookListServeList extends HttpServlet{
	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
	}
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		super.destroy();
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			processRequest(req, resp);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		try {
			processRequest(req, resp);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();
		BookDao bookDao = new BookDao();
		List<Book> books = bookDao.getBooks();
		
		out.println("<html>");
		out.println("<head>");
		out.println("<title>Book List</title>");
		out.println("</head>");
		out.println("<body>");
		out.println("<h4>");
		out.println("Book list");
		out.println("</h4>");
		out.println("<table border = 1>");
		out.println("<thead>\r\n"
				+ "                <tr>\r\n"
				+ "                    <td>id</td>\r\n"
				+ "                    <td>Name</td>\r\n"
				+ "                    <td>Author</td>\r\n"
				+ "                    <td>Subject</td>\r\n"
				+ "                    <td>Price</td>\r\n"
				+ "                    <td>Edit</td>\r\n"
				+ "                    <td>Delete</td>\r\n"
				+ "                </tr>\r\n"
				+ "            </thead>");
		out.println("<tbody>");
		for (Book book : books) {
			out.println("<tr>");
			out.println("<td>" + book.getBookId()+"</td>");
			out.println("<td>" + book.getBookName()+"</td>");
			out.println("<td>" + book.getAuthorName()+"</td>");
			out.println("<td>" + book.getSubjectName()+"</td>");
			out.println("<td>" + book.getPrice()+"</td>");
			out.println("<td>" + "<a href = 'editbook'>Edit</a>"+"</td>");
			out.printf("<td>" + "<a href = 'deletebook?id=%d'>Delete</a>"+"</td>", book.getBookId());
			out.println("</tr>");
		}
		out.println("</tbody>");
		out.println("</table>");
		out.println("<form action = 'addbook'>");
		out.println("<br></br><input type = 'submit' value = 'Add New Book'/>");
		out.println("</form>");
		out.println("<a href='logout'>Sign Out</a>");
		out.println("</body>");
		out.println("</html>");
		
	}
}
