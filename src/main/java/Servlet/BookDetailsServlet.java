package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.BookDao;
import pojo.Book;
@WebServlet("/details")
public class BookDetailsServlet extends HttpServlet {
	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
	}
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		super.destroy();
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			processRequest(req, resp);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		try {
			processRequest(req, resp);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		String subject = req.getParameter("subject");
		
		int id  = Integer.parseInt(req.getParameter("bookid"));
		BookDao bookDao = new BookDao();
		Book book = bookDao.getBook(id);

		PrintWriter out = resp.getWriter();
		resp.setContentType("text/html");
		
		out.println("<html>");
		out.println("<head>");
		Cookie[] arr = req.getCookies();
		for (Cookie cookie : arr) {
			if(cookie.getName().equals("uname")){
				out.printf("<h1>Welcome %s</h1> \n", cookie.getValue());
			}
		}
		out.println("<title>Book Details</title>");
		out.println("</head>");
		out.println("<body>");
		if(book == null)
			out.println("Book not found.");
		else {
			out.println("Name: " + book.getBookName() + "<br/>");
			out.println("Author: " + book.getAuthorName() + "<br/>");
			out.println("Subject: " + book.getSubjectName() + "<br/>");
			out.println("Price: " + book.getPrice() + "<br/>");
		}
		out.println("<form method='post' action='books'>");
		out.printf("<input type='hidden' name='subject' value='%s'/>\r\n", subject);
		out.println("<input type='submit' value='Back'/>");
		out.println("</form>");
		out.println("</body>");
		out.println("</html>");
		}
}
