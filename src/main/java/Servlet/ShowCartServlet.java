package Servlet;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BookDao;
import dao.CustomerDao;
import pojo.Book;
@WebServlet("/showcart")
public class ShowCartServlet extends HttpServlet{
	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
	}
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		super.destroy();
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			processRequest(req, resp);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		try {
			processRequest(req, resp);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		HttpSession session = req.getSession();
		List<Integer> bookids = (List<Integer>) session.getAttribute("cart");
		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();
		BookDao bookDao = new BookDao();
		
			out.println("<html>");
			out.println("<head>");
			out.println("<title>Book Cart</title>");
			out.println("</head>");
			out.println("<body>");
			float total = 0;
			out.println("<table border='1'>");
			out.println("<thead>");
			out.println("<tr>");
			out.println("<th>Id</th>");
			out.println("<th>Name</th>");
			out.println("<th>Author</th>");
			out.println("<th>Subject</th>");
			out.println("<th>Price</th>");
			out.println("</tr>");
			out.println("</thead>");
			out.println("<tbody>");
			
			for (Integer integer : bookids) {
				Book book = bookDao.getBook(integer);
				total = total + book.getPrice();
				out.println("<tr>");
			 	out.printf("<td>%d</td>\r\n", book.getBookId());
				out.printf("<td>%s</td>\r\n", book.getBookName());
				out.printf("<td>%s</td>\r\n", book.getAuthorName());
				out.printf("<td>%s</td>\r\n", book.getSubjectName());
				out.printf("<td>%.2f</td>\r\n", book.getPrice());
				out.println("</tr>");
			}
			out.println("<tr>");
			out.printf("<td colspan = 4>total</td>");
			out.printf("<td>%.02f</td>", total);
			out.println("</tr>");
			out.println("</tbody>");
			out.println("</table>");
			out.println("<a href = 'logout'>Logout</a>");
			out.println("</body>");
			out.println("</html>");
	}
}
