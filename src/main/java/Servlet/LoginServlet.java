package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;



import dao.CustomerDao;
import pojo.Customer;

@WebServlet("/login")
public class LoginServlet extends HttpServlet{
	
	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
	}
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		super.destroy();
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			processRequest(req, resp);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		try {
			processRequest(req, resp);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		PrintWriter out = resp.getWriter();
		resp.setContentType("text/html");
		
		String inputEmail =  req.getParameter("email");
		String inputPassword = req.getParameter("password");
		boolean authenticate = false;
		CustomerDao custDao = new CustomerDao();
		if(custDao.getCustomer(inputEmail) != null) {
			if(custDao.getCustomer(inputEmail).getPassword().equals(inputPassword)) {
				authenticate = true;
			}
		}
	
		Cookie c = new Cookie("uname", custDao.getCustomer(inputEmail).getName());
		resp.addCookie(c);
		Customer cust = null;
		if(authenticate) {
			HttpSession session = req.getSession();
			session.setAttribute("cart", new ArrayList<Integer>());
			cust = custDao.getCustomer(inputEmail);
			if(cust.getName().equals("admin")){
				resp.sendRedirect("admin");
			} else {
				resp.sendRedirect("subject");
			}
		}else {
			out.println("<html>");
			out.println("<head>");
			out.println("<title>Login failed</title>");
			out.println("</head>");
			out.println("<body>");
			out.println("<h1>Invalid user name or password</h1>");
			out.println("<a href = 'index.html'> Login again</a>");
			out.println("</body>");
			out.println("</html>");
		}
		
	}
}
