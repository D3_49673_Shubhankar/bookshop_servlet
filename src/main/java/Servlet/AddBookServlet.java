package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
@WebServlet("/addbook")
public class AddBookServlet extends HttpServlet {
	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
	}
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		super.destroy();
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			processRequest(req, resp);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		try {
			processRequest(req, resp);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();
		out.println("<html>");
		out.println("<head>");
		out.println("<title>Add book</title>");
		out.println("</head>");
		out.println("<body>");
		out.println("<h5>Add Book</h5>");
		out.println("<form action = 'addtodb'>");
		out.println("<input type = 'text' name = 'bookname' placeholder = 'Book Name'/>");
		out.println("<br></br>");
		out.println("<input type = 'text' name = 'authorname' placeholder = 'Author Name'/>");
		out.println("<br></br>");
		out.println("<input type = 'text' name = 'subject' placeholder = 'Subject Name'/>");
		out.println("<br></br>");
		out.println("<input type = 'text' name = 'price' placeholder = 'Price' pattern = '[+-]?([0-9]*[.])?[0-9]+'/>");
		out.println("<br></br>");
		out.println("<input type = 'submit' value = 'Submit' />");
		out.println("</form>");
		out.println("</body>");
		out.println("</html>");
	}
}
