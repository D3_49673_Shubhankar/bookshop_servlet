package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.BookDao;
import dao.CustomerDao;

@WebServlet("/subject")
public class SubjectServlet extends HttpServlet {
	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
	}
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		super.destroy();
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			processRequest(req, resp);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		try {
			processRequest(req, resp);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		PrintWriter out = resp.getWriter();
		resp.setContentType("text/html");
			
		BookDao bookDao = new BookDao();
		
		List<String> subjects = bookDao.getSubejcts();
		
		
		out.println("<html>");
		out.println("<head>");
		Cookie[] arr = req.getCookies();
		for (Cookie cookie : arr) {
			if(cookie.getName().equals("uname")){
				out.printf("<h1>Welcome %s</h1> \n", cookie.getValue());
			}
		}
		out.println("<title>Welcome</title>");
		out.println("</head>");
		out.println("<body>");
		out.println("<h5>Select Subject</h5>");
		out.println("<form method = 'get' action = 'books'>");
		for (String subject : subjects) {
			out.printf("<input type = 'radio' name = 'subject' value = '%s'  />\n %s <br></br>", subject, subject);
		}
		out.println("<input type = 'submit' value = 'show books'/>");
		out.println("<a href = 'showcart' val = 'showcart'>showcart</a>");
		out.println("</form>");
		out.println("</body>");
		out.println("</html>");
		}
}
