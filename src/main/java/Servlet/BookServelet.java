package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.BookDao;
import pojo.Book;

@WebServlet("/books")
public class BookServelet extends HttpServlet{
	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
	}
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		super.destroy();
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			processRequest(req, resp);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		try {
			processRequest(req, resp);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		String subject = req.getParameter("subject");
		
		BookDao bookDao = new BookDao();
		bookDao.getBooks();
		
		List<Book> books = bookDao.getBooks(subject); 
		
		PrintWriter out = resp.getWriter();
		resp.setContentType("text/html");
		out.println("<html>");
		out.println("<head>");
		Cookie[] arr = req.getCookies();
		for (Cookie cookie : arr) {
			if(cookie.getName().equals("uname")){
				out.printf("<h1>Welcome %s</h1> \n", cookie.getValue());
			}
		}
		out.println("<title>Welcome</title>");
		out.println("</head>");
		out.println("<body>");
		out.println("<h5>Select book</h5>");
		out.println("<form method = 'get' action = 'addcart'>");
		for (Book book : books) {
			out.printf("<input type = 'checkbox' value = '%d' name = 'bookid'/> %s \n "
					+ "<a href = 'details?bookid=%d&subject=%s'>Details</a> "
					+"<br></br>", book.getBookId(), book.getBookName(), book.getBookId(), book.getSubjectName());
		}
		out.println("<input type = 'submit' value = 'add to cart'/>");
		out.println("</form>");
		out.println("</body>");
		out.println("</html>");
		}
}
