package Servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.BookDao;
import pojo.Book;

@WebServlet("/addtodb")
public class AddBookToDb extends HttpServlet{
	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
	}
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		super.destroy();
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			processRequest(req, resp);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		try {
			processRequest(req, resp);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();
		
		String bookname = req.getParameter("bookname");
		String authorname = req.getParameter("authorname");
		String subject = req.getParameter("subject");
		float price = Float.parseFloat(req.getParameter("price"));
		
		BookDao bookdau = new BookDao();
		
		Book book = new Book(subject, bookname, authorname, price );
		
		bookdau.insert(book);
		resp.sendRedirect("admin");
	}
}
